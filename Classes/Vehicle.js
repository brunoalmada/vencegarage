﻿function Vehicle() {
    this.licensePlate = '';
    this.name = name;
    this.color = '';
}

Vehicle.prototype.getLicensePlate = function () {
    return this.licensePlate;
}

Vehicle.prototype.getName = function () {
    return this.name;
}

Vehicle.prototype.getColor = function () {
    return this.color;
}

Vehicle.prototype.setLicensePlate = function(value){
    this.licensePlate = value;
}

Vehicle.prototype.setName = function (value) {
    this.name = value;
}

Vehicle.prototype.setColor = function (value) {
    this.color = value;
}