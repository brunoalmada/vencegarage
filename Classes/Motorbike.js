﻿function Motorbike(name) {
    this.name = name;
}

function Motorbike(name, color, licensePlate) {
    this.name = name;
    this.color = color;
    this.licensePlate = licensePlate;
}

Motorbike.prototype = new Vehicle();
Motorbike.prototype.constructor = Motorbike;
Motorbike.prototype.super = Vehicle;