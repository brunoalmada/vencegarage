﻿function Parking(level, maxSpace) {
    this.level = level;
    this.maxSpace = maxSpace;
    this.spaces = [];
}

Parking.prototype.AddVehicle = function (vehicle) {
    //First, check if vehicle is an object
    if (typeof (vehicle) != 'object') {
        console.error('vehicle is not an object');
        return;
    }

    //then, check if the object is a vehicle
    if (vehicle.super !== Vehicle) {
        console.error('the object is not a vehicle');
        return;
    }

    //check if this object already exists in the list
    if (this.spaces.indexOf(vehicle) > -1) {
        console.error('this vehicle is already at the garage on the level ' + this.level);
        return;
    }

    //check if it reached the max number of free spaces
    if (this.spaces.length >= this.maxSpace) {
        console.error('this vehicle cannot be added, because it is reached the max space for this level');
        return;
    }

    this.spaces.push(vehicle);
    console.log('Vehicle added to the garage');
}

Parking.prototype.RemoveVehicle = function (vehicle) {
    //First, check if vehicle is an object
    if (!typeof (vehicle) == 'object') {
        console.error('vehicle is not an object');
        return;
    }

    //check if this object does not exist in the garage
    if (this.spaces.indexOf(vehicle) < 0) {
        console.error('this vehicle was not found on the level ' + this.level);
        return;
    }

    //remove the vehicle from the space
    this.spaces.splice(this.spaces.indexOf(vehicle), 1);
    console.log('Vehicle removed from the garage');
}

Parking.prototype.getLevel = function() {
    return this.level;
}

Parking.prototype.setLevel = function (value) {
    this.level = value;
}

Parking.prototype.getMaxSpace = function () {
    return this.maxSpace;
}

Parking.prototype.setMaxSpace = function (value) {
    this.maxSpace = value;
}