﻿function Garage() {
    this.levels = [];
}

Garage.prototype.AddParkingLevel = function (parkingLevel) {
    //First, check if it is an object
    if (!typeof (parkingLevel) == 'object') {
        console.error('parkingLevel is not an object');
        return;
    }

    //check if this object already exists in the list
    if (this.levels.indexOf(parkingLevel) > -1) {
        console.error('this parking level is already added');
        return;
    }

    this.levels.push(parkingLevel);
}

Garage.prototype.RemoveParkingLevel = function (parkingLevel) {
    this.levels.splice(this.levels.indexOf(parkingLevel), 1);
}

Garage.prototype.getLevels = function () {
    return this.levels;
}