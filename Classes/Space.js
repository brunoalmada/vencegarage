﻿function Space(idNumber) {
    this.number = idNumber;
    this.vehicle = null;
}

Space.prototype.AddVehicle = function (vehicle) {
    //First, check if vehicle is an object
    if (typeof (vehicle) != 'object') {
        console.error('vehicle is not an object');
        return;
    }

    //then, check if the object is a vehicle
    if (vehicle.super !== Vehicle) {
        console.error('the object is not a vehicle');
        return;
    }

    this.vehicle = vehicle;
}

Space.prototype.RemoveVehicle = function (vehicle) {
    //First, check if vehicle is an object
    if (typeof (vehicle) != 'object') {
        console.error('vehicle is not an object');
        return;
    }

    //then, check if the object is a vehicle
    if (vehicle.super !== Vehicle) {
        console.error('the object is not a vehicle');
        return;
    }

    //check if the license plate is not equal
    if (vehicle.getLicensePlate() != this.vehicle.getLicensePlate()) {
        console.error('the license plate is not equal to the vehicle assigned to this space');
        return;
    }

    this.vehicle = null;
}