﻿function Car(name) {
    this.name = name;
}

function Car(name, color, licensePlate) {
    this.name = name;
    this.color = color;
    this.licensePlate = licensePlate;
}

Car.prototype = new Vehicle();
Car.prototype.constructor = Car;
Car.prototype.super = Vehicle;