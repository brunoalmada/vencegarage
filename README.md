# README #

The city of Vence needs a support to implement a new parking garage system. And that's a small solution for it.

Do you know something good to improve this application? That's awesome, I'm looking forward for your ideas! Just fork this repository and send me a pull request.

### What is this repository for? ###

* This small project uses AngularJS, to show the power of this wonderful tool :)
* 1.0

### How do I get set up? ###

* This project uses AngularJS, jQuery and Twitter Bootstrap. But don't worry, it all set up with CDN source ;)
* Of course, in a production environment, minify .js .css, and using sprites makes all sense, doensn't? I'll bring minified .js to you very soon :(

### Who do I talk to? ###

* Bruno Almada
* * brunoha@gmail.com
* * [My LinkedIn profile](http://linkedin.com/in/brunoalmada)
* * [My Xing profile](https://www.xing.com/profile/Bruno_HeitzmannAlmada)
* * [about.me](https://about.me/brunoha)