﻿(function () {
    var app = angular.module('VenceGarage', []);

    this.venceGarage = new Garage();
    this.vehicleList = [];

    app.controller('LevelController', function () {
        this.tab = 1;
        this.parkingList = [];
        this.currentParking = {};

        this.selectItem = function (newItem) {
            this.tab = newItem;
        };

        this.isSelected = function (checkItem) {
            return this.tab === checkItem;
        };

        this.addLevel = function () {
            var newL = new Parking(this.currentParking.level, this.currentParking.spaces);
            this.parkingList.push(newL);
            venceGarage.AddParkingLevel(newL);
            this.currentParking = {};
            this.selectItem(1);
        };
    });

    app.controller('VehicleController', ['$scope', function ($scope) {
        this.tab = 1;
        this.carsList = [];
        this.motorcycleList = [];
        this.currentVehicle = {};

        this.selectItem = function (newItem) {
            this.tab = newItem;
        }

        this.isSelected = function (checkItem) {
            return this.tab === checkItem;
        }

        this.addCar = function (newCar) {
            this.carsList.push(newCar);
            vehicleList.push(newCar);
        };

        this.addMotorcycle = function (newMotorcycle) {
            this.motorcycleList.push(newMotorcycle);
            vehicleList.push(newMotorcycle);
        }

        this.addVehicle = function () {
            //Check if it is a car
            if ($scope.currentTypeVehicle == '1') {
                this.addCar(new Car(this.currentVehicle.name, this.currentVehicle.color, this.currentVehicle.licensePlate));
            }

            //Check if it is a motorcycle
            if ($scope.currentTypeVehicle == '2') {
                this.addMotorcycle(new Motorbike(this.currentVehicle.name, this.currentVehicle.color, this.currentVehicle.licensePlate));
            }

            this.currentVehicle = {};
            this.selectItem(1);
        }
    }]);

    app.controller('ParkingController', function () {
        this.tab = 1;
        this.parkingStatus = [];
        this.levelList = venceGarage.getLevels();
        this.vList = vehicleList;

        this.selectItem = function (newItem) {
            this.tab = newItem;
        };

        this.isSelected = function (checkItem) {
            return this.tab === checkItem;
        };


    });
})();